package com.goostree.codewars;

public class FiboDecrypt {
	public String decrypt(String encrypted, String password) {
		this.chars = encrypted.toCharArray();
		
		undoFiboRotate(password);
				
		return extractMessage(password);
	}
	
	private void undoFiboRotate(String str) {
		int[] fibs = getFiboSequence(str.length());
		
		//rotate characters by fibs -- condense to single pass
		for(int i = fibs.length - 1; i >= 0; i--) {
			for(int j = 0; j < chars.length; j++) {
				if(j % 2 == 0) {
					reverse(j, fibs[i]);
				} else {
					advance(j, fibs[i]);
				}
			}
		}
	}
	
	private int[] getFiboSequence(int x) {
		int[] arr = new int[x];
		
		for(int i = 0; i < x; i++) {
			if(i == 0 || i == 1) {
				arr[i] = 1;
			} else {
				arr[i] = arr[i - 1] + arr[i - 2];
			}
		}
		
		return arr;
	}
	
	private void advance(int i, int x) {
		int zeroVal = chars[i] - asciiOffsetFromZero;
		int newVal = (zeroVal + x) % modVal;
		chars[i] = (char) (newVal + asciiOffsetFromZero);
	}
	
	private void reverse(int i, int x) {
		int zeroVal = chars[i] - asciiOffsetFromZero;
		int newVal = (zeroVal - x);
		
		if(newVal < minChar) {
			newVal += maxChar + 1;
		}
		
		chars[i] = (char) (newVal + asciiOffsetFromZero);
	}
	
	private String extractMessage(String password) {
		String str = String.valueOf(chars);
		return new StringBuilder(str.substring(0, str.length() - password.length())).reverse().toString();
	}
	
	private char[] chars;
	
	private static int asciiOffsetFromZero = 97;
	private static int minChar = 0;
	private static int maxChar = 25;
	private static int modVal = 26;
}
