package com.goostree.codewars.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.goostree.codewars.FiboDecrypt;

public class FiboDecryptTest {
	@Test
	public void testDecrypt() throws Exception {
		FiboDecrypt fibo = new FiboDecrypt();
		System.out.println(fibo.decrypt("ubhewihlz", "pass"));
		assertEquals("encrypted text = fwutaypuv, password = fibo", "tandy",
	        fibo.decrypt("fwutaypuv", "fibo"));
	    assertEquals("encrypted text = mxucyjiijatffk, password = puzzle", "codewars",
	        fibo.decrypt("mxucyjiijatffk", "puzzle"));
	}
}
