Welcome to the C.W spy trainning program. Lets start with little cryptography -

Fibonacci encryption and decryption

A password encryption algorithm on a pair of source message (plain text) and password, containing lowercase letters only (for our convenient), is explained through example as below:

Source Message is: plain

Password is: pass

Steps in the Encryption Algorithm

The source message is reversed -> plain is written as nialp. The password is appended -> the cipher text becomes nialppass. The Fibonacci series, up to the length of the password is generated. In our case, the password length is 4, so the first four Fibonacci terms viz. 1, 1, 2, 3 are generated. Start the Fibonacci series from 1 instead of 0. Every element in odd position in the cipher text is forwarded by the current Fibonacci term, i.e. in the first iteration all odd indexed elements would be advanced by 1, in the second iteration by 1, in the third iteration by 2 and so on. Once 'z' is reached, we wrap around i.e. if we forward 'z' by 1 the output would be 'a'.

Similarly, every element in even position in the cipher text is reversed by the current Fibonacci term, i.e. in the first iteration all even elements would be reversed by 1, in the next iteration by 1, in the next by 2 and so on. Once 'a' is reached, wrap around i.e. if we reverse 'a' by 1 the output would be 'z'. So, in our case the Encryption iteration for the cipher text 'nialppass' would be, ohbkqobrt when the current fibonacci term is 1 pgcjrncqu when the current fibonacci term is 1 reehtleow when the current fibonacci term is 2 ubhewihlz when the current fibonacci term is 3 So, our resultant encrypted string would be 'ubhewihlz'.

Decryption operation is the reverse of encryption operation. For e.g. if encrypted text is 'ubhewihlz', and password is pass, then the decryption will reveal that the text is 'plain'.

Your task would be to decrypt a given list of strings based on two inputs, viz. encrypted text and password. the output should be the original plain text.

